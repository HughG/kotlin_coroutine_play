import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.flow

/**
 * Flow-based quicksort, which uses recursion (of course) but not divide-and-conquer (that is, everything only runs on
 * one coroutine, so it will never act as multi-threaded, no matter what [CoroutineDispatcher] you use).
 */
@UseExperimental(ExperimentalCoroutinesApi::class)
class FlowQuickSort<T : Comparable<T>> : CoroutineQuickSort<T, Flow<T>, FlowCollector<T>>() {
    override fun CoroutineScope.runSort(block: suspend FlowCollector<T>.() -> Unit): Flow<T> = flow(block = block)

    override suspend fun FlowCollector<T>.putValue(value: T) = emit(value)

    override suspend fun recurse(scope: CoroutineScope, block: suspend CoroutineScope.() -> Unit) {
        scope.block()
    }
}

fun <T : Comparable<T>> CoroutineScope.flowQuickSort(list: MutableList<T>) =
    FlowQuickSort<T>().coroQSort(this, list)

/**
 * ChannelFlow-based quicksort, using divide-and-conquer.
 */
@UseExperimental(ExperimentalCoroutinesApi::class)
class ChannelFlowQuickSort<T : Comparable<T>> : CoroutineQuickSort<T, Flow<T>, ProducerScope<T>>() {
    override fun CoroutineScope.runSort(block: suspend ProducerScope<T>.() -> Unit): Flow<T> = channelFlow(block = block)

    override suspend fun ProducerScope<T>.putValue(value: T) = send(value)
}

fun <T : Comparable<T>> CoroutineScope.channelFlowQuickSort(list: MutableList<T>) =
    ChannelFlowQuickSort<T>().coroQSort(this, list)
