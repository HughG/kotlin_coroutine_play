import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeoutOrNull
import kotlin.random.Random
import kotlin.system.measureTimeMillis

@UseExperimental(InternalCoroutinesApi::class, ExperimentalCoroutinesApi::class)
fun main() {
    val rand = Random(23)
    val numbers = List(29) { rand.nextInt(10, 100) }

    sort("Eager:", numbers, ::qSort)

    sort("Lazy sequential, converted to List (so doesn't look lazy):", numbers) {
        runBlocking { flowQuickSort(it).toList() }
    }

    sort("Lazy sequential, as Flow, taking first few:", numbers) {
        runBlocking {
            val sorted = flowQuickSort(it)
            sorted
                .take(10)
                .collect { println(it) }
        }
    }

    // Because we're using divide and conquer, this returns items in the order in which they reach their final position!
    sort("Lazy with divide-and-conquer, as ChannelFlow, taking first few:", numbers) {
        runBlocking {
            val sorted = channelFlowQuickSort(it)
            sorted
                .take(10)
                .collect { println(it) }
        }
    }

    sort("Lazy sequential using thread pool:", numbers) {
        runBlocking {
            val sorted = flowQuickSort(it).flowOn(Dispatchers.Default)
            sorted
                .take(10)
                .collect { println(it) }
        }
    }

    sort("Lazy divide-and-conquer using thread pool:", numbers) {
        runBlocking {
            val sorted = channelFlowQuickSort(it).flowOn(Dispatchers.Default)
            sorted
                .take(10)
                .collect { println(it) }
        }
    }

    sort("Lazy concurrent divide-and-conquer, using thread pool for send _and_ receive:", numbers) {
        runBlocking {
            launch(Dispatchers.Default) {
                val sorted = channelQuickSort(it)

                withTimeoutOrNull(500) {
                    repeat(2) { workerId ->
                        sorted.consumeEach {
                            log("worker ${workerId}: ${it}")
                            delay(50)
                        }
                    }
                }
            }
        }
    }

    // This basically does nothing because the whole thing is launched in the global scope, like a daemon thread.
    // This bypasses structured concurrency and means that the main thread exits before the sort gets a chance to run!
    sort("Lazy concurrent divide-and-conquer, with thread pool, non-blocking launch:", numbers) {
        GlobalScope.launch {
            launch(Dispatchers.Default) {
                val sorted = channelQuickSort(it)

                withTimeoutOrNull(500) {
                    repeat(2) { workerId ->
                        sorted.consumeEach {
                            log("worker ${workerId}: ${it}")
                            delay(50)
                        }
                    }
                }
            }
        }
    }
}

private fun <T> sort(label: String, numbers: List<T>, sort: (MutableList<T>) -> Unit) {
    val time = measureTimeMillis {
        println()
        println(label)
        println(numbers)
        val mutableNumbers = MutableList(numbers.size) { numbers[it] }
        sort(mutableNumbers)
        println(mutableNumbers)
    }
    println("Took ${time} ms")
}
