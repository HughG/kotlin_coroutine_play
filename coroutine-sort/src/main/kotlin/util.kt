fun <T> MutableList<T>.swap(i: Int, j: Int) {
    val temp = this[i]
    this[i] = this[j]
    this[j] = temp
}

fun <T> Iterable<T>.masking(start: Int, end: Int) = mapIndexed { i, v ->
    if (i in start..end) v else null
}

fun log(msg: String) = println("[${Thread.currentThread().name}] $msg")
