import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * Generalised coroutine-based quicksort.  This could just be a single function with local functions, rather than in a
 * class, but I'm using a class as a workaround for <https://youtrack.jetbrains.com/issue/KT-30041>.
 */

@UseExperimental(ExperimentalCoroutinesApi::class)
abstract class CoroutineQuickSort<T : Comparable<T>, TConduit, TScope> {
    protected abstract fun CoroutineScope.runSort(block: suspend TScope.() -> Unit): TConduit

    protected abstract suspend fun TScope.putValue(value: T)

    protected open suspend fun recurse(scope: CoroutineScope, block: suspend CoroutineScope.() -> Unit) {
        scope.launch(block = block)
    }

    private suspend fun MutableList<T>.incAndSwap(firstIndex: Int, secondIndex: Int): Int {
        val newFirstIndex = firstIndex + 1
        swap(newFirstIndex, secondIndex)
        delay(5)
        return newFirstIndex
    }

    private suspend fun partition(list: MutableList<T>, start: Int, end: Int): Int {
        val pivot = list[end]
        var firstIndex = start - 1
        for (secondIndex in start until end) {
            if (list[secondIndex] <= pivot) {
                firstIndex = list.incAndSwap(firstIndex, secondIndex)
            }
        }
        return list.incAndSwap(firstIndex, end)
    }

    // Takes a closed interval [start, end].
    private suspend fun TScope.quickSort(list: MutableList<T>, start: Int, end: Int) {
        when {
            start == end -> putValue(list[start])
            start > end -> return
            else -> {
                val index = partition(list, start, end)

                log("[${start}, ${index}, ${end}] in ${list
                    .masking(start, end)
                    .joinToString(separator = " ") { it?.toString() ?: ".." }}"
                )

                coroutineScope {
                    recurse(this) { quickSort(list, start, index - 1) }
                    putValue(list[index])
                    recurse(this) { quickSort(list, index + 1, end) }
                }
            }
        }
    }


    fun coroQSort(scope: CoroutineScope, list: MutableList<T>): TConduit {
        return scope.runSort {
            quickSort(list, 0, list.size - 1)
        }
    }
}
