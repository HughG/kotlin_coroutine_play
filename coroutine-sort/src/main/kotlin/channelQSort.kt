import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce

/**
 * Producer-based quicksort.
 */
@UseExperimental(ExperimentalCoroutinesApi::class)
class ChannelQuickSort<T : Comparable<T>> : CoroutineQuickSort<T, ReceiveChannel<T>, ProducerScope<T>>() {
    override fun CoroutineScope.runSort(block: suspend ProducerScope<T>.() -> Unit): ReceiveChannel<T> =
        produce(block = block)

    override suspend fun ProducerScope<T>.putValue(value: T) = send(value)
}

fun <T : Comparable<T>> CoroutineScope.channelQuickSort(list: MutableList<T>) =
    ChannelQuickSort<T>().coroQSort(this, list)
