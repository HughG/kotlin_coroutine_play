

/**
 * Sequential quicksort.
 */

fun <T : Comparable<T>> qSort(list: MutableList<T>) {
    fun incAndSwap(firstIndex: Int, secondIndex: Int): Int {
        val newFirstIndex = firstIndex + 1
        list.swap(newFirstIndex, secondIndex)
        return newFirstIndex
    }

    fun partition(list: MutableList<T>, start: Int, end: Int): Int {
        val pivot = list[end]
        var firstIndex = start - 1
        for (secondIndex in start until end) {
            if (list[secondIndex] <= pivot) {
                firstIndex = incAndSwap(firstIndex, secondIndex)
            }
        }
        return incAndSwap(firstIndex, end)
    }

    // Takes a closed interval [start, end].
    fun quickSort(list: MutableList<T>, start: Int, end: Int) {
        if (start >= end) return

        val index = partition(list, start, end)

        log("[${start}, ${index}, ${end}] in ${list
            .masking(start, end)
            .joinToString(separator = " ") { it?.toString() ?: ".." }}"
        )

        quickSort(list, start, index - 1)
        quickSort(list, index + 1, end)
    }

    quickSort(list, 0, list.size - 1)
}
