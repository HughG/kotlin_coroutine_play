package org.tameter.kotlin_coroutine_play

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class DemoApplicationTests {

	@Test
	fun contextLoads() {
		// Test passes if Spring init doesn't fail.
	}

}
