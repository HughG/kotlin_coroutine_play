package org.tameter.kotlin_coroutine_play.events

import org.springframework.integration.annotation.Gateway
import org.springframework.integration.annotation.MessagingGateway
import org.springframework.messaging.handler.annotation.Payload


/**
 * Handles the **sending** and **receipt** of [Events][Event].
 */
@MessagingGateway
interface EventGateway {
    /**
     * Sends the specified [Event] through the &quot;outgoingEvents&quot; `channel`.
     *
     * @param event The `Event` to be sent.
     */
    @Gateway(requestChannel = EventChannelConfiguration.CHANNEL_NAME)
    fun sendEvent(@Payload event: Event)
}
