package org.tameter.kotlin_coroutine_play.events

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.channel.QueueChannel
import org.springframework.messaging.MessageChannel

@Configuration
class EventChannelConfiguration {
    companion object {
        const val CHANNEL_NAME = "outgoingEvents"
    }

    @Bean(CHANNEL_NAME)
    fun outgoingEvents(): QueueChannel = QueueChannel()
}
