package org.tameter.kotlin_coroutine_play.events

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.ProducerScope
import org.tameter.kotlin_coroutine_play.util.AutoLogging
import org.tameter.kotlin_coroutine_play.util.getLogger
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.flow
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Primary
import org.springframework.integration.channel.QueueChannel
import org.springframework.messaging.Message
import org.springframework.stereotype.Service

@Service("eventService")
interface EventService {
    fun publishEvent(event: Event)

    fun querySubscriptionForever(id: Int): Flow<Event>

    fun querySubscriptionCurrentOrNext(id: Int, timeout: Long = 10_000L): Flow<Event>

    fun querySubscriptionCurrentOrNextWithAsync(id: Int, timeout: Long = 10_000L): Flow<Event>
}

@Service
@Primary
class SimpleEventService(
    private val eventGateway: EventGateway,
    @Qualifier(EventChannelConfiguration.CHANNEL_NAME) private val messageChannel: QueueChannel // or SubscribableChannel?
) : EventService {
    companion object : AutoLogging {
        private val logger = getLogger()
    }

    override fun publishEvent(event: Event) {
        eventGateway.sendEvent(event)
    }

    override fun querySubscriptionForever(id: Int) = flow<Event> {
        while (true) {
            val message = messageChannel.receive()
            emitPayloadIfEvent(message)
        }
    }

    override fun querySubscriptionCurrentOrNext(id: Int, timeout: Long): Flow<Event> = flow<Event> {
        logger.info("Query subscription(2) for ${id}, timeout ${timeout}")
        val messages = messageChannel.clear().ifEmpty {
            logger.info("Message channel was was empty")
            messageChannel.receive(timeout)?.let { listOf(it) } ?: emptyList()
        }
        logger.info("Messages = ${messages}")
        for (message in messages) {
            emitPayloadIfEvent(message)
        }
    }

    @UseExperimental(ExperimentalCoroutinesApi::class)
    override fun querySubscriptionCurrentOrNextWithAsync(id: Int, timeout: Long): Flow<Event> =
        channelFlow<Event> {
            logger.info("Query subscription(2) for ${id}, timeout ${timeout}")
            val messages = async {
                messageChannel.clear().ifEmpty {
                    logger.info("Message channel was was empty")
                    messageChannel.receive(timeout)?.let { listOf(it) } ?: emptyList()
                }
            }.await()
            logger.info("Messages = ${messages}")
            for (message in messages) {
                sendPayloadIfEvent(message)
            }
        }

    private suspend fun FlowCollector<Event>.emitPayloadIfEvent(
        message: Message<*>?
    ) {
        val payload = message?.payload as? Event
        logger.info("Received ${payload}")
        if (payload != null) {
            emit(payload)
            logger.info("... emitted ${payload}")
        }
    }

    @UseExperimental(ExperimentalCoroutinesApi::class)
    private suspend fun ProducerScope<Event>.sendPayloadIfEvent(
        message: Message<*>?
    ) {
        val payload = message?.payload as? Event
        logger.info("Received ${payload}")
        if (payload != null) {
            send(payload)
            logger.info("... emitted ${payload}")
        }
    }
}