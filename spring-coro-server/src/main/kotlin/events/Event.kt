package org.tameter.kotlin_coroutine_play.events

data class Event(val content: String)
