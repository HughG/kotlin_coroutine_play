package org.tameter.kotlin_coroutine_play.events

import org.tameter.kotlin_coroutine_play.util.AutoLogging
import org.tameter.kotlin_coroutine_play.util.getLogger
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import kotlin.random.Random

@Component
class EventGenerator(
    private val eventService: EventService
) {
    companion object : AutoLogging {
        private val logger = getLogger()
    }

    private val rand = Random(23)

    @Scheduled(fixedDelay = 5000)
    fun generateEvent() {
        val content = rand.nextInt(100, 999).toString()
        logger.info("Publishing '${content}'")
        eventService.publishEvent(Event(content))
        logger.info("... published '${content}'")
    }
}