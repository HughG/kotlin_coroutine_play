package org.tameter.kotlin_coroutine_play.events

import org.tameter.kotlin_coroutine_play.util.AutoLogging
import org.tameter.kotlin_coroutine_play.util.getLogger
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class EventController(
    private val eventService: EventService
) {
    companion object : AutoLogging {
        private val logger = getLogger()
    }

    @FlowPreview
    @GetMapping("/flowAll/{id}")
    fun getEventsFlowAll(@PathVariable id: Int): Flow<Event> {
        logger.info("Getting events for '${id}'")
        return eventService.querySubscriptionForever(id)
    }

    @FlowPreview
    @GetMapping("/flowNext/{id}")
    fun getEventsFlowNext(@PathVariable id: Int): Flow<Event> {
        logger.info("Getting events for '${id}'")
        return eventService.querySubscriptionCurrentOrNext(id)
    }

    // Hitting this endpoint gets the exception in getEventsFlowNextWithAsync_exception.txt
    @FlowPreview
    @GetMapping("/flowNextWA/{id}")
    fun getEventsFlowNextWithAsync(@PathVariable id: Int): Flow<Event> {
        logger.info("Getting events for '${id}'")
        return eventService.querySubscriptionCurrentOrNextWithAsync(id)
    }

    @FlowPreview
    @GetMapping("/flowNextBlocking/{id}")
    fun getEventsFlowNextBlocking(@PathVariable id: Int): Flow<Event> {
        logger.info("Getting events for '${id}'")
        return runBlocking {
            eventService.querySubscriptionCurrentOrNext(id)
        }
    }

    @FlowPreview
    @GetMapping("/flowNextAsList/{id}")
    fun getEventsFlowNextAsList(@PathVariable id: Int): List<Event> {
        logger.info("Getting events for '${id}'")
        return runBlocking {
            eventService.querySubscriptionCurrentOrNext(id).toList()
        }
    }

    @UseExperimental(ExperimentalCoroutinesApi::class)
    @FlowPreview
    @GetMapping("/list/{id}")
    fun getEventsList(@PathVariable id: Int): List<Event> {
        logger.info("Getting events for '${id}'")
        return runBlocking {
            eventService.querySubscriptionForever(id).buffer(10).toList()
        }
    }
}